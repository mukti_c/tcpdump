package com.example.tcpdump;

/**
 * Created by muktichowkwale on 13/01/15.
 */
public class GlobalVariables {
    public static PastConnQueue last100Conn;
    public static LastTwoSecQueue lastTwoSec;
    public static long startTime = 0;
    public static long endTime = 0;
    public static String connSourceIP = null;
    public static String connDestIP = null;
    public static int connSourcePort = 0;
    public static int connDestPort = 0;
    public static String connProtocol = null;
    public static String connService = null;

    public static void clearVar() {
        startTime = 0;
        endTime = 0;
        connSourceIP = null;
        connSourcePort = 0;
        connDestIP = null;
        connDestPort = 0;
        connProtocol = null;
        connService = null;
    }
}
