package com.example.tcpdump;

/*
 * Creates an object for the connection record with the KDD Cup '99 data set features.
 * Of the 41 features in the KDD Cup '99 data set, features 10 to 22 have been removed as they
 * are not related to smartphones. Hence, this object will contain 28 features.
 */

import java.io.File;
import java.io.FileWriter;
import java.util.Set;

public class KDDConnection {

    // Features as described in the KDD Cup '99 Documentation
    // List of features and descriptions at: http://www.sc.ehu.es/acwaldap/gureKddcup/README.pdf

    // Intrinsic features
    int duration = 0;
    String protocol = null;
    String service = null;
    String flag = null;
    int src_bytes = 0;
    int dst_bytes = 0;
    byte land = 0;
    int wrong_fragment = 0;
    int urgent = 0;

    // Time traffic features
    int count = 0;
    int srv_count = 0;
    double serror_rate = 0.00;
    double srv_serror_rate = 0.00;
    double rerror_rate = 0.00;
    double srv_error_rate = 0.00;
    double same_srv_rate = 0.00;
    double diff_srv_rate = 0.00;
    double srv_diff_host_rate = 0.00;

    // Machine traffic features
    int dst_host_count = 0;
    int dst_host_srv_count = 0;
    double dst_host_same_srv_rate = 0.00;
    double dst_host_diff_srv_rate = 0.00;
    double dst_host_same_src_port_rate = 0.00;
    double dst_host_srv_diff_host_rate = 0.00;
    double dst_host_serror_rate = 0.00;
    double dst_host_srv_serror_rate = 0.00;
    double dst_host_rerror_rate = 0.00;
    double dst_host_srv_error_rate = 0.00;

    private String convertRecord() {
        return (this.duration
                + "," + this.protocol
                + ',' + this.service
                + ',' + this.flag
                + ',' + this.src_bytes
                + ',' + this.dst_bytes
                + ',' + this.land
                + ',' + this.wrong_fragment
                + ',' + this.urgent
                + ',' + this.count
                + ',' + this.srv_count
                + ',' + String.format("%.2f", this.serror_rate)
                + ',' + String.format("%.2f", this.srv_serror_rate)
                + ',' + String.format("%.2f", this.rerror_rate)
                + ',' + String.format("%.2f", this.srv_error_rate)
                + ',' + String.format("%.2f", this.same_srv_rate)
                + ',' + String.format("%.2f", this.diff_srv_rate)
                + ',' + String.format("%.2f", this.srv_diff_host_rate)
                + ',' + this.dst_host_count
                + ',' + this.dst_host_srv_count
                + ',' + String.format("%.2f", this.dst_host_same_srv_rate)
                + ',' + String.format("%.2f", this.dst_host_diff_srv_rate)
                + ',' + String.format("%.2f", this.dst_host_same_src_port_rate)
                + ',' + String.format("%.2f", this.dst_host_srv_diff_host_rate)
                + ',' + String.format("%.2f", this.dst_host_serror_rate)
                + ',' + String.format("%.2f", this.dst_host_srv_serror_rate)
                + ',' + String.format("%.2f", this.dst_host_rerror_rate)
                + ',' + String.format("%.2f", this.dst_host_srv_error_rate));
    }

    public static void writeToARFF(String filename, KDDConnection object) {
        // Filename is the name of the ARFF file to which the connection record is to be appended.
        try {
            File file = new File(filename);
            FileWriter writer = new FileWriter(file, true);
            writer.append("\n" + object.convertRecord());
            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Creates a connection
    public static void createConnectionRecord(Set<DataFromLog> logData) {
        KDDConnection newConn = new KDDConnection();

        // TIMESTAMP is in milliseconds
        newConn.duration = (int) (GlobalVariables.endTime - GlobalVariables.startTime);

        newConn.protocol = GlobalVariables.connProtocol;
        newConn.service = GlobalVariables.connService;
        newConn.flag = getFlags(newConn.protocol);

        if (GlobalVariables.connSourceIP.equals(GlobalVariables.connDestIP) && GlobalVariables.connSourcePort == GlobalVariables.connDestPort) {
            newConn.land = 1;
        } else {
            newConn.land = 0;
        }

        for (DataFromLog temp1: logData) {
            newConn.src_bytes += (temp1.SRC_IP.equals(GlobalVariables.connSourceIP)) ? temp1.LENGTH : 0;
            newConn.dst_bytes += (temp1.DEST_IP.equals(GlobalVariables.connSourceIP)) ? temp1.LENGTH : 0;
            newConn.wrong_fragment += (temp1.CHECKSUM_DESC != null && temp1.CHECKSUM_DESC.equals("correct")) ? 0 : 1;
            newConn.urgent += (temp1.FLAGS.URG) ? 1 : 0;
        }

        // Create ReducedKDDConnection object and pass & add to last100Conn and lastTwoSec
        ReducedKDDConnection tempConn = new ReducedKDDConnection();
        tempConn.TIMESTAMP = GlobalVariables.endTime;
        tempConn.PROTOCOL = newConn.protocol;
        tempConn.SERVICE = newConn.service;
        tempConn.FLAG = newConn.flag;
        tempConn.DEST_IP = GlobalVariables.connDestIP;
        tempConn.SRC_PORT = GlobalVariables.connSourcePort;
        tempConn.DEST_PORT = GlobalVariables.connDestPort;
        newConn = PastConnQueue.calculateTrafficFeatures(tempConn, newConn, GlobalVariables.last100Conn);
        newConn = LastTwoSecQueue.calculateTrafficFeatures(tempConn, newConn, GlobalVariables.lastTwoSec);
        writeToARFF(ReadFile1.csvFile, newConn);
        GlobalVariables.last100Conn.addConn(tempConn);
        GlobalVariables.lastTwoSec.addConn(tempConn);
    }

    private static String getFlags (String protocol) {
        if (protocol.equals("tcp")) {

        }
        else if (protocol.equals("udp")) {
            
        }
        return "OTH";
    }
}