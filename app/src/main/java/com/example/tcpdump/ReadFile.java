package com.example.tcpdump;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.TextView;

import org.jnetpcap.Pcap;
import org.jnetpcap.packet.JPacket;
import org.jnetpcap.packet.JPacketHandler;
import org.jnetpcap.packet.format.FormatUtils;
import org.jnetpcap.protocol.network.Icmp;
import org.jnetpcap.protocol.network.Ip4;
import org.jnetpcap.protocol.network.Ip6;
import org.jnetpcap.protocol.tcpip.Tcp;
import org.jnetpcap.protocol.tcpip.Udp;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by muktichowkwale on 11/01/15.
 */

public class ReadFile extends Activity {

    private final static String TAG = "ReadFile";
    private final static String filename = Environment.getExternalStorageDirectory().getAbsoluteFile()+File.separator+"dump.pcap";
    private final static String csvFile = Environment.getExternalStorageDirectory().getAbsolutePath()+File.separator+"connection.csv";

    private TextView text;
    private ArrayList<JPacket> packets = null;
    private ArrayList<DataFromLog> logData = null;
    private int numPackets = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reader);

        text = (TextView) findViewById (R.id.title_text);
        packets = new ArrayList<JPacket>();
        logData = new ArrayList<DataFromLog>();

        File file = new File(csvFile);
        try {file.createNewFile();}
        catch (Exception e) {e.printStackTrace();}
        getPackets();
        displayStaticLog(text);
    }

    private void getPackets() {

        Log.d (TAG, "in getPackets()");
        StringBuilder errbuff = new StringBuilder();

        // Open parser in offline mode
        final Pcap parser = Pcap.openOffline(filename, errbuff);
        if (parser == null) {
            Log.d (TAG, errbuff.toString());
        } else {
            Log.d(TAG, "Opened pcap file");

            JPacketHandler<String> handler = new JPacketHandler<String>() {

                @Override
                public void nextPacket(JPacket packet, String user) {
                    // Add packet to displayArray and extract packet data
                    if (packet != null) {
                        Log.d(TAG, "time started: " + System.nanoTime());
                        packets.add(packet);
                        logData.add(numPackets, new DataFromLog());
                        getPacketData(packet, logData.get(numPackets));

                        // Add packet to the queues
                        GlobalVariables.last100Conn.addConn(logData.get(numPackets));
                        GlobalVariables.lastTwoSec.addConn(logData.get(numPackets));

                        // Convert to ReducedKDDConnection and write to file
                        ReducedKDDConnection.writeToARFF(csvFile, ReducedKDDConnection.createConnectionRecord(logData.get(numPackets)));
                        //Log.d(TAG, ReducedKDDConnection.convertRecord(ReducedKDDConnection.createConnectionRecord(logData.get(numPackets))));
                        numPackets++;
                    }
                }
            };

            parser.loop(-1, handler, null);
            parser.close(); }
    }

    private void getPacketData (JPacket packet, DataFromLog data) {

        // Types of packets that can be detected
        Ip4 ip4 = new Ip4();
        Ip6 ip6 = new Ip6();
        Tcp tcp = new Tcp();
        Udp udp = new Udp();
        Icmp icmp = new Icmp();

        if (packet != null) {
            data.TIMESTAMP = new Date(packet.getCaptureHeader().timestampInMillis()).getTime();

            if (packet.hasHeader(ip4.ID)) {
                packet.getHeader(ip4);
                data.SRC_IP = FormatUtils.ip(ip4.source());
                data.DEST_IP = FormatUtils.ip(ip4.destination());
                data.FLAGS = getFlags(packet);

                // Check which protocol is being used
                if (packet.hasHeader(tcp.ID)) {
                    packet.getHeader(tcp);
                    data.PROTOCOL = "tcp";
                    data.SRC_PORT = tcp.source();
                    data.DEST_PORT = tcp.destination();
                    data.LENGTH = tcp.getPayloadLength() + tcp.getHeaderLength();
                    data.CHECKSUM = tcp.checksum();
                    data.CHECKSUM_DESC = tcp.checksumDescription();
                }

                else if (packet.hasHeader(udp.ID)) {
                    packet.getHeader(udp);
                    data.PROTOCOL = "udp";
                    data.SRC_PORT = udp.source();
                    data.DEST_PORT = udp.destination();
                    data.LENGTH = udp.getPayloadLength() + udp.getHeaderLength();
                    data.CHECKSUM = udp.checksum();
                    data.CHECKSUM_DESC = udp.checksumDescription();
                }
            }

            else if (packet.hasHeader(icmp.ID)) {
                packet.getHeader(icmp);
                data.PROTOCOL = "icmp";
                data.SRC_IP = "unknown";
                data.DEST_IP = "unknown";
                data.SRC_PORT = -1;
                data.DEST_PORT = -1;
                data.LENGTH = icmp.getPayloadLength() + icmp.getHeaderLength();
                data.FLAGS = getFlags(packet);
                data.CHECKSUM = icmp.checksum();
                data.CHECKSUM_DESC = icmp.checksumDescription();
            }

            else if (packet.hasHeader(ip6.ID)) {
                packet.getHeader(ip6);
                data.SRC_IP = FormatUtils.asStringIp6(ip6.source(), true);
                data.DEST_IP = FormatUtils.asStringIp6(ip6.destination(), true);
                data.FLAGS = getFlags(packet);

                // Check which protocol is being used
                if (packet.hasHeader(tcp.ID)) {
                    packet.getHeader(tcp);
                    data.PROTOCOL = "tcp";
                    data.SRC_PORT = tcp.source();
                    data.DEST_PORT = tcp.destination();
                    data.LENGTH = tcp.getPayloadLength() + tcp.getHeaderLength();
                    data.CHECKSUM = tcp.checksum();
                    data.CHECKSUM_DESC = tcp.checksumDescription();
                }

                else if (packet.hasHeader(udp.ID)) {
                    packet.getHeader(udp);
                    data.PROTOCOL = "udp";
                    data.SRC_PORT = udp.source();
                    data.DEST_PORT = udp.destination();
                    data.LENGTH = udp.getPayloadLength() + udp.getHeaderLength();
                    data.CHECKSUM = udp.checksum();
                    data.CHECKSUM_DESC = udp.checksumDescription();
                }
            }
        }
    }

    private Flags getFlags (JPacket packet) {
        Tcp tcp = new Tcp();
        Udp udp = new Udp();
        Icmp icmp = new Icmp();
        Flags flags = new Flags();

        if (packet.hasHeader(tcp.ID)) {
            packet.getHeader(tcp);
            flags.ACK = tcp.flags_ACK();
            flags.CWR = tcp.flags_CWR();
            flags.ECE = tcp.flags_ECE();
            flags.FIN = tcp.flags_FIN();
            flags.PSH = tcp.flags_PSH();
            flags.RST = tcp.flags_RST();
            flags.SYN = tcp.flags_SYN();
            flags.URG = tcp.flags_URG();
        }

        else if (packet.hasHeader(udp.ID) || packet.hasHeader(icmp.ID)) {
            flags.none = true;
        }
        return flags;
    }

    public void displayStaticLog (TextView textView) {

        int i;
        int size = logData.size();

        for (i = 0; i < size; i++) {

            textView.append("\n--------------------------------------------\n");
            textView.append("\nDirection: " + logData.get(i).IN
                    + "\nSource IP Address: " + logData.get(i).SRC_IP
                    + "\nSource Port Number: " + logData.get(i).SRC_PORT
                    + "\nDestination IP Address: " + logData.get(i).DEST_IP
                    + "\nDestination Port Number: " + logData.get(i).DEST_PORT
                    + "\nLength: " + logData.get(i).LENGTH
                    + "\nProtocol: " + logData.get(i).PROTOCOL
                    + "\nChecksum: " + logData.get(i).CHECKSUM
                    + "\nTime: " + logData.get(i).TIMESTAMP);
        }
    }
}